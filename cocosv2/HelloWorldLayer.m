//
//  HelloWorldLayer.m
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/04.
//  Copyright tatsuaki watanabe 2014年. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "GameLayer.h"
#import <GameKit/GameKit.h>

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        CGSize size = [[CCDirector sharedDirector] winSize];

        ccColor4B color = {70,180,255,255};
        CCLayerColor *colorLayer = [CCLayerColor layerWithColor:color];
        [self addChild:colorLayer];
        
        CCSprite *background = [CCSprite spriteWithFile:@"haikei2.png"];
        background.position = ccp(size.width/2,size.height/2);
        [background.texture setAliasTexParameters];
        background.scaleX = size.width/background.contentSize.width;
        background.scaleY = size.height/background.contentSize.height;
        [self addChild:background];
        
        balloon = [CCSprite spriteWithFile:@"lily0.png"];
        balloon.position = ccp(size.width/2,size.height*3/5);
        balloon.scale = 1.3f;
        [self addChild:balloon];
        
        //キャラクターアクション開始
        CCAnimation *animObj = [CCAnimation animation];
        for(int i = 0; i < 4; i++)[animObj addSpriteFrameWithFilename:[NSString stringWithFormat:@"lily%1d.png",i]];
        
        animObj.loops = -1;
        animObj.delayPerUnit = 0.5;
        animAction = [CCAnimate actionWithAnimation:animObj];
        [balloon runAction:animAction];//anime start
        
        CCMoveTo* moveBaloon = [CCMoveTo actionWithDuration:1
                                                   position:ccp(balloon.position.x,balloon.position.y-10)];
        CCCallFuncN* balloonDownMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(balloonDownMoveFinished:)];
        [balloon runAction:[CCSequence actions:moveBaloon, balloonDownMoveDidFinish, nil]];
        
        CCSprite *title = [CCSprite spriteWithFile:@"title.png"];
        title.position = ccp(size.width/2,size.height*5/6);
        [title.texture setAliasTexParameters];
        title.scale = 5.0f;
        [self addChild:title];
    
        //画像の場合はこちら
        CCMenuItem * item = [CCMenuItemImage itemWithNormalImage:@"play.png" selectedImage:@"play.png" target:self selector:@selector(pushButton:)];
        CCMenu * menu1  = [CCMenu menuWithItems:item, nil];
        item.tag=1;
        [menu1 alignItemsHorizontallyWithPadding:20];
        [menu1 setPosition:ccp(size.width/2, size.height*2/6)];
        [self addChild:menu1];
        
        CCMenuItem * item1 = [CCMenuItemImage itemWithNormalImage:@"rate.png" selectedImage:@"rate.png" target:self selector:@selector(pushButton:)];
        CCMenuItem * item2 = [CCMenuItemImage itemWithNormalImage:@"tweet.png" selectedImage:@"tweet.png" target:self selector:@selector(pushButton:)];
        CCMenuItem * item3 = [CCMenuItemImage itemWithNormalImage:@"lb.png" selectedImage:@"lb.png" target:self selector:@selector(pushButton:)];
        item1.tag=2;
        item2.tag=3;
        item3.tag=4;
        CCMenu * menu2  = [CCMenu menuWithItems:item1,item2,item3, nil];
        [menu2 alignItemsHorizontallyWithPadding:20];
        [menu2 setPosition:ccp(size.width/2, size.height/6)];
        [self addChild:menu2];
   
        //LeaderBoard用 Tweet View
        _viewController = [[UIViewController alloc] init];
        [[[CCDirector sharedDirector] view] addSubview:_viewController.view];
        TW_viewController = [[UIViewController alloc] init];
        [[[CCDirector sharedDirector] view] addSubview:TW_viewController.view];
        
        //SE
        NSString *path = [[NSBundle mainBundle] pathForResource:@"pi" ofType:@"mp3"];
        NSURL *url = [NSURL fileURLWithPath:path];
        pi = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [pi prepareToPlay];
        
        //high score初期値設定(初回ゲーム起動時)
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
        [defaults setObject:@"0" forKey:@"highscore"];
        [ud registerDefaults:defaults];
        ud = [NSUserDefaults standardUserDefaults];
        hs = [ud integerForKey:@"highscore"];
    
        GKLocalPlayer* player = [GKLocalPlayer localPlayer];
        player.authenticateHandler = ^(UIViewController* ui, NSError* error ){
            if( ui != nil ){
                NSLog(@"leaderboard errorrrr");
            } else {
                NSLog(@"leaderboard ok");
            }
        };
 	}
	return self;
}

- (void)balloonDownMoveFinished:(id)sender {
    CCMoveTo* moveBaloon = [CCMoveTo actionWithDuration:1
                                               position:ccp(balloon.position.x,balloon.position.y+10)];
    CCCallFuncN* balloonUpMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(balloonUpMoveFinished:)];
    [balloon runAction:[CCSequence actions:moveBaloon, balloonUpMoveDidFinish, nil]];
}

- (void)balloonUpMoveFinished:(id)sender {
    CCMoveTo* moveBaloon = [CCMoveTo actionWithDuration:1
                                               position:ccp(balloon.position.x,balloon.position.y-10)];
    CCCallFuncN* balloonDownMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(balloonDownMoveFinished:)];
    [balloon runAction:[CCSequence actions:moveBaloon, balloonDownMoveDidFinish, nil]];
}

- (void)pushButton:(id)sender
{
    switch([sender tag]){
        case 1:
            [pi release];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene] ]];
            break;
            
        case 2:
            [pi play];
            CCLOG(@"rate");
            NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=898646260&mt=8&type=Purple+Software"];
            [[UIApplication sharedApplication] openURL:url];
            break;
        case 3:
            [pi play];
            CCLOG(@"tweet");
            // ビューコントローラの初期化
            TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
            
            [tweetViewController setInitialText:[NSString stringWithFormat:@"Flying Lilyでハイスコア%d!みんなもやってみて！ I just scored %d in Flying Lily! Let's play with your iPhone! http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=898646260&mt=8&type=Purple+Software",hs,hs]];
            [tweetViewController addImage:[UIImage imageNamed:@"tweet_add.png"]];
            
            tweetViewController.completionHandler = ^(TWTweetComposeViewControllerResult res) {
                if (res == TWTweetComposeViewControllerResultCancelled) {
                    NSLog(@"キャンセル");
                }
                else if (res == TWTweetComposeViewControllerResultDone) {
                    NSLog(@"成功");
                }
                [TW_viewController dismissModalViewControllerAnimated:YES];
            };
            [TW_viewController presentModalViewController:tweetViewController animated:YES];

            break;
        case 4:
            [pi play];
            CCLOG(@"LB pushed high score is %d",hs);
            GKGameCenterViewController *gcView = [GKGameCenterViewController new];
            
            if (gcView != nil){
                if(hs > 0){
                    GKScore *scoreReporter = [[GKScore alloc] initWithCategory:@"score"];
                    scoreReporter.value = hs;
                    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
                        if (error != nil)NSLog(@"error %@",error);
                    }];
                };
                gcView.gameCenterDelegate = self;
                gcView.viewState = GKGameCenterViewControllerStateLeaderboards;
                [_viewController presentViewController:gcView animated:YES completion:nil];
            };
            break;
        default:
            CCLOG(@"???");
            break;
 }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [_viewController dismissViewControllerAnimated:YES completion:nil];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

#pragma mark GameKit delegate

//com.t-tu.ios.flylily${PRODUCT_NAME:rfc1034identifier}

@end
