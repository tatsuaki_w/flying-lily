//
//  AppDelegate.h
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/04.
//  Copyright tatsuaki watanabe 2014年. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import <iAd/iAd.h>


// Added only for iOS 6 support
@interface MyNavigationController : UINavigationController <CCDirectorDelegate>
@end

@interface AppController : NSObject <UIApplicationDelegate>
{
	UIWindow *window_;
	MyNavigationController *navController_;

	CCDirectorIOS	*director_;							// weak ref
}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) MyNavigationController *navController;
@property (readonly) CCDirectorIOS *director;

@end
