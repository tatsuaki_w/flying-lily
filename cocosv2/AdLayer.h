//
//  AdLayer.h
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/06.
//  Copyright (c) 2014年 tatsuaki watanabe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>
#import "cocos2d.h"
#import "GADBannerView.h"

@interface AdLayer : CCLayer <ADBannerViewDelegate, GADBannerViewDelegate>
{
    
}

+ (id)layer;
@end