//
//  GameLayer.m
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/04.
//  Copyright (c) 2014年 tatsuaki watanabe. All rights reserved.
//

#import "GameLayer.h"
#import "HelloWorldLayer.h"
#import <AudioToolbox/AudioToolbox.h>
#import <Math.h>
#import "AdLayer.h"

@implementation GameLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
    //広告ロード
    AdLayer *adlay = [AdLayer node];
    [scene addChild:adlay];

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
        
        //game balance
        bitweenCrow = 230;
        bless = 1.5;
        gravity = 0.1;
        maxSpeed = 2;
        positionPM = maxSpeed;//初期値
        crowSec = 1.7;
        crowScore = 9;
        
        size = [[CCDirector sharedDirector] winSize];
        //背景
        ///　背景色
        ccColor4B color = {0,0,114,255};
        CCLayerColor *colorLayer = [CCLayerColor layerWithColor:color];
        [self addChild:colorLayer z:1];
        
        background = [CCSprite spriteWithFile:@"haikei2.png"];
        background.position = ccp(size.width/2,size.height/2);
        [background.texture setAliasTexParameters];
        background.scaleX = size.width/background.contentSize.width;
        background.scaleY = size.height/background.contentSize.height;
        [self addChild:background z:1];

        background2 = [CCSprite spriteWithFile:@"haikei_hoshi.png"];
        background2.position = ccp(size.width/2,size.height+size.height/2-5);
        [background2.texture setAliasTexParameters];
        background2.scaleX = size.width/background2.contentSize.width;
        background2.scaleY = size.height/background2.contentSize.height;
        [self addChild:background2 z:1];
        
        //キャラクタ
        balloon = [CCSprite spriteWithFile:@"lily0.png"];
        balloon.position = ccp(50, 50+balloon.contentSize.height/2);
        [self addChild:balloon z:4];
        
        //地面
        ground = [CCSprite spriteWithFile:@"haikei_ki.png"];
        ground.scaleX = size.width/ground.contentSize.width;
        ground.scaleY = size.height/ground.contentSize.height;
        ground.position = ccp(size.width/2,size.height/2);
        [self addChild:ground z:2];
        
         self.isTouchEnabled = YES;
        
        //マイクinit
        AudioStreamBasicDescription dataFormat;
        dataFormat.mSampleRate = 44100.0f;
        dataFormat.mFormatID = kAudioFormatLinearPCM;
        dataFormat.mFormatFlags = kLinearPCMFormatFlagIsBigEndian | kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
        dataFormat.mBytesPerPacket = 2;
        dataFormat.mFramesPerPacket = 1;
        dataFormat.mBytesPerFrame = 2;
        dataFormat.mChannelsPerFrame = 1;
        dataFormat.mBitsPerChannel = 16;
        dataFormat.mReserved = 0;
        AudioQueueNewInput(&dataFormat, AudioInputCallback, (__bridge void *)(self), CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &queue);
        AudioQueueStart(queue, NULL);
        
        UInt32 enabledLevelMeter = true;
        AudioQueueSetProperty(queue,kAudioQueueProperty_EnableLevelMetering,&enabledLevelMeter,sizeof(UInt32));
        
        [NSTimer scheduledTimerWithTimeInterval:0.1
                                         target:self
                                       selector:@selector(updateVolume:)
                                       userInfo:nil
                                        repeats:YES];
        
        AudioSessionInitialize(NULL, NULL, NULL, NULL);
        AudioSessionSetActive(YES);
        UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
        AudioSessionSetProperty(kAudioSessionProperty_AudioCategory,
                                sizeof (sessionCategory),
                                &sessionCategory);

        //画像の場合はこちら
        CCMenuItem * item = [CCMenuItemImage itemWithNormalImage:@"play.png" selectedImage:@"play.png" target:self selector:@selector(pushButton:)];
        item.tag=1;
        menu1  = [CCMenu menuWithItems:item, nil];
        [menu1 alignItemsHorizontallyWithPadding:20];
        [menu1 setPosition:ccp(size.width/2, size.height*2/6)];
        [self addChild:menu1 z:0];
        [menu1 setEnabled:(false)];
        
        //menu
        CCMenuItem * item1 = [CCMenuItemImage itemWithNormalImage:@"rate.png" selectedImage:@"rate.png" target:self selector:@selector(pushButton:)];
        CCMenuItem * item2 = [CCMenuItemImage itemWithNormalImage:@"tweet.png" selectedImage:@"tweet.png" target:self selector:@selector(pushButton:)];
        CCMenuItem * item3 = [CCMenuItemImage itemWithNormalImage:@"lb.png" selectedImage:@"lb.png" target:self selector:@selector(pushButton:)];
        item1.tag=2;
        item2.tag=3;
        item3.tag=4;
        
        menu2  = [CCMenu menuWithItems:item1,item2,item3, nil];
        [menu2 alignItemsHorizontallyWithPadding:20];
        [menu2 setPosition:ccp(size.width/2, size.height/6)];
        [self addChild:menu2 z:0];
        [menu2 setEnabled:(false)];
        
        //LeaderBoard用 Tweet View
        _viewController = [[UIViewController alloc] init];
        [[[CCDirector sharedDirector] view] addSubview:_viewController.view];
        TW_viewController = [[UIViewController alloc] init];
        [[[CCDirector sharedDirector] view] addSubview:TW_viewController.view];

        
        //変数初期化
        state = 0;
        _targets = [[NSMutableArray alloc]init];
        _clouds= [[NSMutableArray alloc]init];
        cal = 0;
        score = 0;
        ground_disp = true;

        //score
//        scoreString = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%03d", score]fntFile:@"unlearne.ttf"];

        scoreString = [NSString stringWithFormat:@"0"];
        scoreLabel = [CCLabelTTF labelWithString:scoreString fontName:@"FAMania" fontSize:100];
        scoreLabel.position = ccp(size.width/2,size.height*5/6);
        [self addChild:scoreLabel z:5];
        
        //BLOW START
        blowImage = [CCSprite spriteWithFile:@"blow.png"];
        blowImage.position = ccp(size.width/2,size.height*2/3);
        [blowImage.texture setAliasTexParameters];
        blowImage.scale = (size.width/blowImage.contentSize.width)*0.8;
        [self addChild:blowImage z:5];
        
        //cal
        calString = [NSString stringWithFormat:@"Burn 0Kcal."];
        calLabel = [CCLabelTTF labelWithString:calString fontName:@"FAMania" fontSize:24];
        calLabel.position = ccp(size.width/2,size.height*4/6);
        [self addChild:calLabel z:0];
        
        //resume
        resumeLabel = [CCLabelTTF labelWithString:@"PRESS RESUME" fontName:@"FAMania" fontSize:24];
        resumeLabel.position = ccp(size.width/2,size.height/2);
        [self addChild:resumeLabel z:0];
        
        //SE preload
        NSString *path = [[NSBundle mainBundle] pathForResource:@"falling" ofType:@"mp3"];
        NSURL *url = [NSURL fileURLWithPath:path];
        fall = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [fall prepareToPlay];
        path = [[NSBundle mainBundle] pathForResource:@"balloon" ofType:@"mp3"];
        url = [NSURL fileURLWithPath:path];
        hit = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [hit prepareToPlay];
        path = [[NSBundle mainBundle] pathForResource:@"jump8" ofType:@"mp3"];
        url = [NSURL fileURLWithPath:path];
        blow = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [blow prepareToPlay];
        path = [[NSBundle mainBundle] pathForResource:@"get" ofType:@"mp3"];
        url = [NSURL fileURLWithPath:path];
        get = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [get prepareToPlay];
        path = [[NSBundle mainBundle] pathForResource:@"pi" ofType:@"mp3"];
        url = [NSURL fileURLWithPath:path];
        pi = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [pi prepareToPlay];
        [pi play];
    }
	return self;
}

//音量を検知
- (void)updateVolume:(NSTimer *)timer {
    AudioQueueLevelMeterState levelMeter;
    UInt32 levelMeterSize = sizeof(AudioQueueLevelMeterState);
    AudioQueueGetProperty(queue,kAudioQueueProperty_CurrentLevelMeterDB,&levelMeter,&levelMeterSize);
    
    //NSLog(@"mPeakPower=%0.9f", levelMeter.mPeakPower);
    //NSLog(@"mAveragePower=%0.9f", levelMeter.mAveragePower);
    
    if (levelMeter.mPeakPower >= -1.0f) {
        [self bress];
    }
}

static void AudioInputCallback(
                               void* inUserData,
                               AudioQueueRef inAQ,
                               AudioQueueBufferRef inBuffer,
                               const AudioTimeStamp *inStartTime,
                               UInt32 inNumberPacketDescriptions,
                               const AudioStreamPacketDescription *inPacketDescs)
{
}

//一定以上の音を検知した時
- (void)bress {
    
    switch (state) {
        case 0:
            //BLOW START
            state = 1;
            blowImage.zOrder = 0;
            
            //キャラクターアクション開始
            CCAnimation *animObj = [CCAnimation animation];
            for(int i = 0; i < 4; i++)[animObj addSpriteFrameWithFilename:[NSString stringWithFormat:@"lily%1d.png",i]];
            
            animObj.loops = -1;
            animObj.delayPerUnit = 0.5;
            animAction = [CCAnimate actionWithAnimation:animObj];
            [balloon runAction:animAction];//anime start
            
            //地面出て消える
            CCMoveTo* moveGround = [CCMoveTo actionWithDuration:8
                                                       position:ccp(size.width/2,-ground.contentSize.height*2)];
            CCCallFuncN* groundMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(groundMoveFinished:)];
            [ground runAction:[CCSequence actions:moveGround, groundMoveDidFinish, nil]];

            //ActionSTART
            [self schedule:@selector(nextFrame:)];
            [self schedule:@selector(addTarget:) interval:crowSec];
            [self schedule:@selector(addCloud:) interval:3.0f];

            break;
        case 1:
            
            if(positionPM < maxSpeed-0.5){
               positionPM = positionPM + bless;
               cal = cal + 0.01;
               //最高速
               if(positionPM > maxSpeed)positionPM = maxSpeed;
            
                //空気音再生
                [blow play];
            }

            break;
            
        default:
            break;
    }
}

//画面タッチを検知した時
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
   
    //test mode
    //[self bress];

    switch (state) {
        case 1:
            //一時停止する
            [self pauseSchedulerAndActions];
            [balloon pauseSchedulerAndActions];
            if(ground_disp)[ground pauseSchedulerAndActions];
            for(CCSprite *crow in _targets)[crow pauseSchedulerAndActions];
            for(CCSprite *cloud in _clouds)[cloud pauseSchedulerAndActions];
            resumeLabel.zOrder = 5;
            calLabel.zOrder = 5;
            state = 2;
            break;
        case 2:
            //一時停止解除
            [self resumeSchedulerAndActions];
            [balloon resumeSchedulerAndActions];
            if(ground_disp)[ground resumeSchedulerAndActions];
            for(CCSprite *crow in _targets)[crow resumeSchedulerAndActions];
            for(CCSprite *cloud in _clouds)[cloud resumeSchedulerAndActions];
            resumeLabel.zOrder = 0;
            calLabel.zOrder = 0;
            state = 1;
        default:
            break;
    }
}

- (void)pushButton:(id)sender
{
    [pi play];
    switch([sender tag]){
        case 1:
            for(CCSprite *crow in _targets)[self removeChild:crow cleanup:YES];
            for(CCSprite *cloud in _clouds)[self removeChild:cloud cleanup:YES];
            [blow release];
            [pi release];
            [fall release];
            [hit release];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene] ]];
            break;
        case 2:
            CCLOG(@"rate");
            NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=898646260&mt=8&type=Purple+Software"];
            [[UIApplication sharedApplication] openURL:url];
            break;
        case 3:
            CCLOG(@"tweet");
            // ビューコントローラの初期化
            TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
            
            [tweetViewController setInitialText:[NSString stringWithFormat:@"Flying Lilyでハイスコア%d!みんなもやってみて！ I just scored %d in Flying Lily! Let's play with your iPhone! http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=898646260&mt=8&type=Purple+Software",hs,hs]];
            [tweetViewController addImage:[UIImage imageNamed:@"tweet_add.png"]];
            
            tweetViewController.completionHandler = ^(TWTweetComposeViewControllerResult res) {
                if (res == TWTweetComposeViewControllerResultCancelled) {
                    NSLog(@"キャンセル");
                }
                else if (res == TWTweetComposeViewControllerResultDone) {
                    NSLog(@"成功");
                }
                [TW_viewController dismissModalViewControllerAnimated:YES];
            };
            [TW_viewController presentModalViewController:tweetViewController animated:YES];
            
            break;
        case 4:
            CCLOG(@"LB pushed");
            
            GKGameCenterViewController *gcView = [GKGameCenterViewController new];
            
            if (gcView != nil){
                if(hs > 0){
                    CCLOG(@"hs is %d",hs);
                    GKScore *scoreReporter = [[GKScore alloc] initWithCategory:@"score"];
                    scoreReporter.value = hs;
                    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
                        if (error != nil)NSLog(@"error %@",error);
                    }];
                };
                gcView.gameCenterDelegate = self;
                gcView.viewState = GKGameCenterViewControllerStateLeaderboards;
                [_viewController presentViewController:gcView animated:YES completion:nil];
            };
            break;
        default:
            CCLOG(@"???");
            break;
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [_viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)addTarget:(ccTime)dt {

    //敵キャラクタ
    if (score < crowScore) {
        CCSprite *crow = [CCSprite spriteWithFile:@"crow.png"];
        int actualY = 150+(arc4random() % ((int)size.height-250));
        crow.position = ccp(size.width+crow.contentSize.width, actualY);
        
        //アクション開始
        CCAnimation *crowAnim = [CCAnimation animation];
        [crowAnim addSpriteFrameWithFilename:[NSString stringWithFormat:@"crow0.png"]];
        [crowAnim addSpriteFrameWithFilename:[NSString stringWithFormat:@"crow1.png"]];
        
        crowAnim.loops = -1;
        crowAnim.delayPerUnit = 0.5;
        crowAction = [CCAnimate actionWithAnimation:crowAnim];
        [crow runAction:crowAction];//anime start
        
        [self addChild:crow z:3];
        [_targets addObject:crow];
    
        CCMoveTo* moveTarget = [CCMoveTo actionWithDuration:2.5
                                               position:ccp(-crow.contentSize.width,actualY)];
        CCCallFuncN* actionForTargetMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(targetMoveFinished:)];
        [crow runAction:[CCSequence actions:moveTarget, actionForTargetMoveDidFinish, nil]];
    } else {
        int actualY = 100+(arc4random() % ((int)size.height/2-100));
        for (int i=0;i<2;i++){
            CCSprite *crow = [CCSprite spriteWithFile:@"crow.png"];
            
            if(i==0)crow.position = ccp(size.width+crow.contentSize.width, actualY);
            else crow.position = ccp(size.width+crow.contentSize.width, actualY+bitweenCrow);
            
            //アクション開始
            CCAnimation *crowAnim = [CCAnimation animation];
            [crowAnim addSpriteFrameWithFilename:[NSString stringWithFormat:@"crow0.png"]];
            [crowAnim addSpriteFrameWithFilename:[NSString stringWithFormat:@"crow1.png"]];
            
            crowAnim.loops = -1;
            crowAnim.delayPerUnit = 0.5;
            crowAction = [CCAnimate actionWithAnimation:crowAnim];
            [crow runAction:crowAction];//anime start
            
            [self addChild:crow z:3];
            [_targets addObject:crow];
            
            CCMoveTo* moveTarget = [CCMoveTo actionWithDuration:2.5
                                                       position:ccp(-crow.contentSize.width,crow.position.y)];
            CCCallFuncN* actionForTargetMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(targetMoveFinished:)];
            [crow runAction:[CCSequence actions:moveTarget, actionForTargetMoveDidFinish, nil]];
        }
    }
}

- (void)addCloud:(ccTime)dt {
    
    //背景の雲
    CCSprite *cloud = [CCSprite spriteWithFile:@"cloud.png"];
    int actualY = 50+(arc4random() % ((int)size.height-250));
    cloud.position = ccp(size.width+cloud.contentSize.width, actualY+cloud.contentSize.height);
    cloud.scale = 4+(arc4random() % 2);
    [cloud setOpacity:128];
    [self addChild:cloud z:2];
    [_clouds addObject:cloud];

    CCMoveTo* moveCloud = [CCMoveTo actionWithDuration:5+(arc4random() % 5)
                                                   position:ccp(-cloud.contentSize.width,actualY+cloud.contentSize.height-50)];
    CCCallFuncN* actionForCloudMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveFinished:)];
    [cloud runAction:[CCSequence actions:moveCloud, actionForCloudMoveDidFinish, nil]];
  
}

- (void)targetMoveFinished:(id)sender {
    CCSprite *crow = (CCSprite *)sender;
    [self removeChild:crow cleanup:YES];
    [_targets removeObject:crow];
    
    if(state==1){
        //score表示
        score = score + 1;
        scoreString = [NSString stringWithFormat:@"%d",score];
        [scoreLabel setString:scoreString];

        [get play];
    }
}

- (void)groundMoveFinished:(id)sender {
    ground_disp = false;
    CCSprite *grd = (CCSprite *)sender;
    [self removeChild:grd cleanup:YES];
}


- (void)cloudMoveFinished:(id)sender {
    CCSprite *cloud = (CCSprite *)sender;
    [self removeChild:cloud cleanup:YES];
    [_clouds removeObject:cloud];
}
    
- (void)balloonMoveFinished:(id)sender {
    CCSprite *bal = (CCSprite *)sender;
    [self removeChild:bal cleanup:YES];
}


- (void)hitMethod:(NSTimer*)timer{
    [balloon setTexture:[[CCTextureCache sharedTextureCache] addImage: @"ballooned.png"]];
    CCMoveTo* moveBaloon = [CCMoveTo actionWithDuration:2
                                               position:ccp(balloon.position.x,-(balloon.contentSize.height+balloon.position.y))];
    CCCallFuncN* balloonMoveDidFinish = [CCCallFuncN actionWithTarget:self selector:@selector(balloonMoveFinished:)];
    [balloon runAction:[CCSequence actions:moveBaloon, balloonMoveDidFinish, nil]];
    
    //落下音再生
    [fall play];
}

- (void)nextFrame:(ccTime)dt {
    //カロリ−表示
    calString = [NSString stringWithFormat:@"Burn %2.2fKcal.",cal];
    [calLabel setString:calString];
    
    switch (state) {
        case 0:
            //play前待機画面
            break;
        case 1:

            //背景の動き
            if(background2.position.y > size.height/2){
                [background setPosition:ccp(background.position.x,background.position.y-0.14)];
                [background2 setPosition:ccp(background2.position.x,background2.position.y-0.14)];
            }
            
            for(CCSprite *crow in _targets){
                //当たり判定
                CGRect balloonRect = CGRectMake(balloon.position.x,balloon.position.y,                     balloon.contentSize.width,balloon.contentSize.height-20);
                CGRect crowRect = CGRectMake(crow.position.x,crow.position.y,crow.contentSize.width,crow.contentSize.height);
//                CCLOG(@"bx:%f,by:%f,bsx:%f,bsy:%f",balloon.position.x,balloon.position.y,balloon.contentSize.width,balloon.contentSize.height);
                //あたった場合
                if(CGRectIntersectsRect(balloonRect,crowRect)){
                    [hit play];
                    state=3;
                }
            }
            
            if(balloon.position.y < -20){
                CCLOG(@"out");
                state = 3;
            } else {
                
                //画面上部にぶつかっているかどうか判定
                if (balloon.position.y > [[CCDirector sharedDirector] winSize].height-100){
                    balloon.position = ccp(balloon.position.x,[[CCDirector sharedDirector] winSize].height-100);
                    positionPM = 0;
                } else {
                    //重力加速度
                    positionPM = positionPM - gravity;
                    balloon.position = ccp(balloon.position.x,balloon.position.y + positionPM);
                }
            }
            
            break;
        case 2:
            //一時停止画面
            break;
            
        case 3:
            //落下or衝突

            //落下アクション
            [balloon stopAllActions];
            [balloon setTexture:[[CCTextureCache sharedTextureCache] addImage: @"balloon_hit.png"]];

            //0.3秒後に落下
            [NSTimer
                scheduledTimerWithTimeInterval:0.3f
                target:self
                selector:@selector(hitMethod:)
                userInfo:nil
                repeats:NO];
            
            //スコア登録
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];  // 取得
            hs = [ud integerForKey:@"highscore"];
            
            if(score > hs)hs = score;
            
            highScoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"HighScore:%3d",hs] fontName:@"FAMania" fontSize:24];
            [ud setInteger:hs forKey:@"highscore"];
            highScoreLabel.position = ccp(size.width/2,size.height*3/6+20);
            [self addChild:highScoreLabel z:5];

            state = 4;

        break;
        case 4:
            //落下アクション後
            menu1.zOrder = 3;
            menu2.zOrder = 3;
            [menu1 setEnabled:(true)];
            [menu2 setEnabled:(true)];
            calLabel.zOrder = 5;
            resumeLabel.zOrder = 0;
            
            break;
        default:
            break;
    }
 
}

@end


//com.t-tu.ios.flylily${PRODUCT_NAME:rfc1034identifier}