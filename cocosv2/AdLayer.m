//
//  AdLayer.m
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/06.
//  Copyright (c) 2014年 tatsuaki watanabe. All rights reserved.
//


#import "AdLayer.h"

@implementation AdLayer
{
    ADBannerView* _adView;
    GADBannerView* _gadView;
    BOOL _bannerIsVisible;
}

+ (id)layer
{
    return [[self alloc] init];
}

- (id)init
{
    if ((self = [super init])) {
        _bannerIsVisible = NO;
        // iAd設定
        _adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
        _adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
        [[[CCDirector sharedDirector] view] addSubview:_adView];
        _adView.frame = CGRectOffset(_adView.frame, 0, -_adView.frame.size.height);
        _adView.delegate = self;

        // AdMob設定
        _gadView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        _gadView.adUnitID = @"ca-app-pub-7240536018506970/1746741049";
        _gadView.rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        [[[CCDirector sharedDirector] view] addSubview:_gadView];
        _gadView.frame = CGRectOffset(_gadView.frame, 0, -_gadView.frame.size.height);
        _gadView.delegate = self;

    }
    return self;
}

- (void)dealloc
{
    [_adView removeFromSuperview];
    [_gadView removeFromSuperview];
    
//    [super dealloc];
}

//iAd広告取得成功時の処理
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!_bannerIsVisible) {
        [UIView animateWithDuration:0.3 animations:^{
            _adView.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        }];
        _bannerIsVisible = YES;
    }
  
}

//iAd広告取得失敗時の処理
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [_adView removeFromSuperview];
    _adView = nil;
   [self loadAdmobViewRequest];
}

//AdMob取得処理
- (void)loadAdmobViewRequest
{
    GADRequest *request = [GADRequest request];
    //以下の端末ではテスト広告をリクエスト
    request.testDevices = [NSArray arrayWithObjects:
                           @"B0BB82FE-1D4E-586C-A1D9-17BB2AEB7357",// シミュレータ
                           @"d95bc42a6c9cdc2e501c9eb2f7df6969137c8e20",//本体
                           nil];
    [_gadView loadRequest:request];
}

//AdMob取得成功
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (!_bannerIsVisible) {
        [UIView animateWithDuration:0.3 animations:^{
            _gadView.frame = CGRectOffset(view.frame, 0, view.frame.size.height);
        }];
        _bannerIsVisible = YES;
    }
}

//AdMob取得失敗
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    [_gadView removeFromSuperview];
    _gadView = nil;
}

@end