//
//  GameLayer.h
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/04.
//  Copyright (c) 2014年 tatsuaki watanabe. All rights reserved.
//
#import <GameKit/GameKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "cocos2d.h"
#import "AdLayer.h"
#import <Twitter/TWTweetComposeViewController.h>
#include <AVFoundation/AVFoundation.h>

@interface GameLayer : CCLayer
{
    CCSprite *balloon;
    CCSprite *background;
    CCSprite *background2;
    CCSprite *blowImage;
    CCAnimate *animAction;
    CCAnimate *crowAction;
    CCSprite *ground;
    NSMutableArray *_targets;
    NSMutableArray *_clouds;
    bool ground_disp;
    float positionPM;
    
    //game balance
    int bitweenCrow; //カラス間の距離
    float bless;//音量検知時の上昇速度
    float gravity;//重力
    float maxSpeed;//最高上昇速度
    float crowSec;//最高上昇速度
    
    //leaderboard
    int hs;
    UIViewController* _viewController;
    UIViewController* TW_viewController;
    
    /*
     zlorder 
     background:1
     cloud&Ground:2
     crow:3
     ballonn:4
     manu:5
     */
    
    //labels
    CCLabelTTF *resumeLabel;
    CCLabelTTF *highScoreLabel;

    //score
    int score;
    CCLabelTTF *scoreLabel;
    NSString *scoreString;
    int crowScore;
//    CCLabelBMFont *scoreString;

    //カロリー
    float cal;
    CCLabelTTF *calLabel;
    NSString *calString;
    
    CGSize size;
    CCMenu *menu1;
    CCMenu *menu2;
    
    /*ゲーム状態
     0:before
     1:play
     2:stop
     3:out
    */
    int state;
    
    //音声認識用
    AudioQueueRef   queue;     // 音声入力用のキュー
    NSTimer         *_timer;    // 監視タイマー
    
    //広告
    ADBannerView *_bannerView;
    
    //効果音
    AVAudioPlayer *fall;
    AVAudioPlayer *hit;
    AVAudioPlayer *blow;
    AVAudioPlayer *pi;
    AVAudioPlayer *get;
}

+(CCScene *) scene;

@end
