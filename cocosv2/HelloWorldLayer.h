//
//  HelloWorldLayer.h
//  cocosv2
//
//  Created by tatsuaki watanabe on 2014/07/04.
//  Copyright tatsuaki watanabe 2014年. All rights reserved.
//


#import <GameKit/GameKit.h>

#include <AVFoundation/AVFoundation.h>
#import <Twitter/TWTweetComposeViewController.h>
// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer <GKGameCenterControllerDelegate>
{
    CCSprite *balloon;
    CCAnimate *animAction;
    int hs;
    UIViewController* _viewController;
    UIViewController* TW_viewController;
    AVAudioPlayer *pi;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
